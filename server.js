var express = require("express");
var app = express();

app.use(express.static('public'))

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(require('./public/js/routing'))

app.listen(process.env.PORT || 3000, function(){
  console.log("Server running at Port 3000");
});
