var express = require("express");
var router = express.Router();
var path = "../public/views/";

router.get("/",function(req, res){
  res.render("../index");
});

router.get("/home",function(req, res){
  res.render(path + "index");
});

router.get("/work",function(req, res){
  res.render(path + "work");
});

router.get("/about",function(req, res){
  res.render(path + "about");
});

router.get("*",function(req, res){
  res.render(path + "404");
});

module.exports = router;
